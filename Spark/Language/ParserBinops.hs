{-# LANGUAGE PatternGuards #-}
module Spark.Language.ParserBinops (
  pRelOp,   pRelApp,   mergeRelOp,
  pBoolOp,  pBoolApp,  mergeBoolOp,
  pArithOp, pArithApp, mergeArithOp, pArithPrefix
  ) where

import Control.Monad
import Control.Applicative hiding (many, (<|>), optional)
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char(letter, digit, hexDigit)
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language

import qualified Spark.Language.AST as S
import qualified Spark.Language.Lexer as L
	
lexer 		  = L.makeTokenParser

reservedOp  = L.reservedOp lexer
reOp        = reservedOp

reserved    = L.reserved lexer
re          = reserved

-----------------------------------------------------------------------------
-- * Relational Operators

pRelOp :: S.RelOp -> Parser ()
pRelOp o = (\(Just x) -> try x <?> "RelOp") (lookup o
  [(S.Eq, reOp "=" >> notFollowedBy (reOp ">")), (S.Neq, reOp "/="),
   (S.Lt, reOp "<" >> notFollowedBy (reOp "=")),
   (S.Lte, reOp "<="), (S.Gt, reOp ">" >> notFollowedBy (reOp "=")), (S.Gte, reOp ">=")])

pRelApp :: S.RelOp -> Parser (S.Exp -> S.Exp -> S.Exp)
pRelApp o = pRelOp o >> return (mergeRelOp o)

mergeRelOp :: S.RelOp -> S.Exp -> S.Exp -> S.Exp
mergeRelOp o (S.RelApp o' es) e1 | True <- o == o' = S.RelApp o (e1:es)
mergeRelOp o e1 e2 = (S.RelApp o [e1, e2])

-----------------------------------------------------------------------------
-- * Boolean Operators

pBoolOp :: S.BoolOp -> Parser ()
pBoolOp o = (\(Just x) -> try x <?> "BoolOp") (lookup o
  [(S.Not, re "not"), (S.And, re "and"), (S.Or, re "or"), (S.Xor, re "xor"),
   (S.AndThen, re "and" >> re "then"), (S.OrElse, re "or" >> re "else"),
   (S.Implies, reOp "->"), (S.Iff, reOp "<->")])

-- "and then" and "or else" operators require special attention.
pBoolApp :: S.BoolOp -> Parser (S.Exp -> S.Exp -> S.Exp)
pBoolApp S.And =
  pBoolOp S.And >> (
    (try (re "then") >> return (mergeBoolOp S.AndThen)) <|>
    (return (mergeBoolOp S.And)) )
pBoolApp S.Or =
  pBoolOp S.Or >> (
    (try (re "else") >> return (mergeBoolOp S.OrElse)) <|>
    (return (mergeBoolOp S.Or)) )
pBoolApp o = pBoolOp o >> return (mergeBoolOp o)

-- R.(S.T) = R.S.T = REOp ROComp [R, S, T]
mergeBoolOp :: S.BoolOp -> S.Exp -> S.Exp -> S.Exp
mergeBoolOp o (S.BoolApp o' es) e1 | True <- o == o' = S.BoolApp o (e1:es)
mergeBoolOp o e1 e2 = (S.BoolApp o [e1, e2])


-----------------------------------------------------------------------------
-- * Arithmetic Operators

pArithOp :: S.ArithOp -> Parser ()
pArithOp o = (\(Just x) -> try x <?> "ArithOp") (lookup o
  [(S.Neg, pminus), (S.Plus, reOp "+"), (S.Minus, pminus),
   (S.Mult, reOp "*"), (S.Div, reOp "/" >> notFollowedBy (reOp "=")), (S.Exp, reOp "**"),
   (S.Abs, re "abs"), (S.Cat, reOp "&"), (S.Mod, re "mod"),
   (S.Rem, re "rem")])
  where
    pminus = reOp "-" >> notFollowedBy (reOp ">")

pArithApp :: S.ArithOp -> Parser (S.Exp -> S.Exp -> S.Exp)
pArithApp o = pArithOp o >> return (mergeArithOp o)

mergeArithOp :: S.ArithOp -> S.Exp -> S.Exp -> S.Exp
mergeArithOp o (S.ArithApp o' es) e1 | True <- o == o' = S.ArithApp o (e1:es)
mergeArithOp o e1 e2 = (S.ArithApp o [e1, e2])

pArithPrefix :: S.ArithOp -> Parser (S.Exp -> S.Exp)
pArithPrefix o = pArithOp o >> return ((S.ArithApp o) . (:[]) )
