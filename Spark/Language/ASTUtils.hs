module Spark.Language.ASTUtils where

import Spark.Language.AST
import Data.Generics.Uniplate.Data(universeBi)
import Data.List(nub)

------------------------------------------------------------------------------
-- * Constructors

-- | Creates an Exp from an Identifier
idenToExp :: Identifier -> Exp
idenToExp i = Prim $ PrimName $ DirectName i

-- | Creates an Exp from a Name
nameToExp :: Name -> Exp
nameToExp n = Prim $ PrimName n

-- | Creates an Exp with an AttributeReference
attrRefExp :: SubtypeMark -> Identifier -> [Exp] -> Exp
attrRefExp t attr args = Prim $ PrimName $ AttrRef t (ADesigIden attr args)

-- | Simple Exit Statement
ifExitStat :: Predicate -> Statement
ifExitStat p = ifThen p $ ExitStat Nothing Nothing

-- | Negates an expression
notExp :: Exp -> Exp
notExp e = BoolApp Not [e]

-- | Conjuction of 2 expressions
andExp :: Exp -> Exp -> Exp
andExp e1 e2 = BoolApp And [e1,e2]

-- | Creates a simple loop, i.e., a loop with no iteration scheme
simpleLoop :: (Maybe Identifier) -> (Maybe Predicate) -> Statement -> Statement
simpleLoop i p sts = LoopStat i Nothing p (mkAssertStat p sts)
  where
    mkAssertStat Nothing sts = sts
    mkAssertStat (Just p) sts = ProofStat Assert p `statSeq` sts

-- | Creates a sequence of statements
statSeq :: Statement -> Statement -> Statement
statSeq (Block l) (Block m) = Block (l ++ m)
statSeq (Block l) m = Block (l ++ [m])
statSeq l (Block m) = Block (l:m)
statSeq s1 s2 = Block [s1,s2]

-- | Creates an If statement only with the Then branch
ifThen :: Predicate -> Statement -> Statement
ifThen p s = IfStat p s [] Nothing

ifTE :: Predicate -> Statement -> Statement -> Statement
ifTE p s1 s2 = IfStat p s1 [] (Just s2)

-- | Receiving an expression it creates a new assert with that expression
assertStat :: Exp -> Statement
assertStat e = ProofStat Assert e

-- | It returns an Exp from an Int
intToExp :: Int -> Exp
intToExp i = Prim $ PrimLit $ LitInt $ NumLit (toInteger i) (show i)

------------------------------------------------------------------------------
-- * Destructors

expToInt :: Exp -> Int
expToInt (Prim (PrimLit (LitInt i))) = (fromIntegral $ nNum i) :: Int

varsOfExp :: Exp -> [Name]
varsOfExp e = nub [ n | (Prim (PrimName n)) <- universeBi e ]

varsOfStat :: Statement -> [Name]
varsOfStat e = nub ([ n | (Prim (PrimName n)) <- universeBi e ]
                    ++ [n | (AssignStat n _) <- universeBi e ])

varsOfStatL :: [Statement] -> [Name]
varsOfStatL e = nub . (foldr (++) []) . (map varsOfStat) $ e

removeAssigns :: Statement -> Exp
removeAssigns (Block b) = BoolApp And (map removeAssigns b)
removeAssigns (AssignStat x e) = RelApp Eq [(Prim (PrimName x)),e]
removeAssigns _ = error "%[ASTUtils.removeAssigns] code must be a basic block to create logic encoding."
