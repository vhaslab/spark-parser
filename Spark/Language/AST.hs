{-# OPTIONS_GHC -funbox-strict-fields #-}
{-# LANGUAGE DeriveDataTypeable #-}
-- |Abstract Syntax Tree data-type definition, for RavenSPARK 
--
--
--  Language specification available at: 
--  <http://www.altran-praxis.com/downloads/SPARK/technicalReferences/SPARK95_RavenSPARK.pdf>
--
-- Some rules synonyms are largely used in the language reference, namely:
--
-- > id            ::= identifier
-- > selector_name ::= name
-- > direct_name   ::= id
-- > defining_id   ::= id
-- > subtype_mark  ::= name
--
-- The rules presented here do not have occurences of such synonyms,
-- we resolved the names making the implementation details clearer.
--
module Spark.Language.AST where

import Control.Applicative
import Data.Generics.Uniplate.Data
import Data.Data
import qualified Data.Map as M

-- |Undefined Data Types
data TODO
  = Nil
   deriving (Eq, Show, Data, Typeable)
  
---------------------------------------------------------------------
-- * Compilation Units

data Unit 
  = UnitLibrary Context LibraryItem
  | UnitSubunit Context Subunit
   deriving (Show, Data, Typeable)
  
type Context = [ContextItem]

data ContextItem
  = ContextItemWith [PackageName]
  | ContextItemUse  [SubtypeMark]
   deriving (Eq, Show, Data, Typeable)
  
data LibraryItem
  = LibItemPackageDecl PackageDecl
  | LibItemPackageBody PackageBody
  | LibItemMainSubprog [PackageName]  -- ^ inherit clause
                       SubprogBody    -- ^ main subprogram
   deriving (Show, Data, Typeable)
                       
data Subunit
  = SubunitSubprogram Name SubprogBody
  | SubunitPackage    Name PackageBody
   deriving (Show, Data, Typeable)
  


---------------------------------------------------------------------
-- * Packages

data PackageName
  = PkgName {
      pkgParent :: !(Maybe PackageName)
    , pkgId     :: !Identifier
  }  deriving (Eq, Show, Data, Typeable)
  
type CompleteName = PackageName

-- |Package Declarations
--
-- Rule 7.1
--
data PackageDecl
  = PackageDecl [PackageName]            -- ^ inherit clause
                !Bool                    -- ^ private flag
                !PackageName
                PackageAnnotation        -- ^ package annotations
                PackageSpecPart          -- ^ package /visible part/
                (Maybe PackageSpecPart)  -- ^ package /private part/
   deriving (Show, Data, Typeable)

-----------------------------------------------------------
-- ** Package annotations

-- SUGGESTION: move own_variables and initializatin_specification in PackageDecl 
-- Rules 7.1.2, 7.1.3, 7.1.4
--
data PackageAnnotation
  = PackageAnnotation [OwnVar]  -- ^ own variables
                      [OwnVar]  -- ^ initialization specification
   deriving (Show, Data, Typeable)

-- Rule 7.1.2, 7.1.3, 9.1 (RavenSpark not implemented), Barnes A1.2 and pag. 279
data OwnVar = OwnVar ![(Mode, Identifier)]
                     (Maybe SubtypeMark)  -- see Barnes pag. 279
             deriving (Show, Data, Typeable)

-----------------------------------------------------------
-- ** Package specification

data PackageSpecPart
  = PackageSpecPart [RenameDecl]      -- ^ renaming_declaration for visible and 
                                      -- private_part
                    [PackageDeclItem] -- ^ package_declarative_item for visible
                                      -- and private part
  | HiddenSpecPart  !PackageName !String  -- ^ caused by a "--# hide " annotation
   deriving (Show, Data, Typeable)

-- | Renaming declaration.
-- 
-- Section 8.5
data RenameDecl
  = PackageRename PackageName    -- ^ old package name
                  PackageName    -- ^ new package name
  | SubprogRename SubprogramSpec -- ^ subprogram_specification
                  CompleteName
   deriving (Show, Data, Typeable)

data PackageDeclItem
  = PackDeclItemBD BasicDeclItem
  | PackDeclItemSD SubprogramDecl
  | PackDeclItemESD SubprogramDecl -- ^ interfacing section 8.1, John Barnes
                                      -- DOUBT: not sure if 
                                      -- we can ignore pragma
   deriving (Show, Data, Typeable)
  
-- | SPARK Basic decl item
-- from Barnes book, page 393
data BasicDeclItem
  = BasicDeclItem  BasicDecl
  | RepresentationClause
  | ProofFunc      SubprogramSpec
  | ProofDecl      ProofDeclaration
   deriving (Show, Data, Typeable)
  
data ProofDeclaration
  = ProofTypeDecl   Identifier
  | ProofTypeAssert Identifier SubtypeMark
   deriving (Show, Data, Typeable)

-----------------------------------------------------------
-- ** SPARK Package Bodies

-- Rule 7.2
data PackageBody 
  = PackageBody PackageName                -- ^ parent name and package name
                [RefinementClause]         -- ^ refinment_definition
                DeclarativePart
                Statements
   deriving (Show, Data, Typeable)

-- |SPARK Package Refinements
--
-- Rule 7.2.1
--
data RefinementClause
  = RefinementClause Identifier            -- ^ subject
                    [(Mode, PackageName)] -- ^ constituent_list
   deriving (Show, Data, Typeable)

-----------------------------------------------------------
-- * Subprograms

-- Rule 6.1
--
data SubprogramDecl
  = SubprogramDecl !SubprogramKind
                   SubprogramSpec  -- ^ specification
                   SubprogramAnnot -- ^ annotations
   deriving (Show, Data, Typeable)

data SubprogramKind = Procedure | Function 
   deriving (Show, Data, Typeable)

-- |SPARK Subprograms Calls
--
-- Rule 6.4
data SubprogCall 
  = SubprogCall Name               -- ^ procedure_name
                ActualParameters
   deriving (Eq, Show, Data, Typeable)

data ActualParameters 
  = NamedParams    [NamedParam]     -- ^ named_parameters
  | ExplicitParams [Exp]            -- ^ positional_parameter_assoc_list
   deriving (Eq, Show, Data, Typeable)

-----------------------------------------------------------
-- ** Subprogram Specification
data SubprogramSpec
  = SubprogramSpec {
      spsName   :: !Identifier       -- ^ subprogram name
     ,spsParams :: [SubprogramParam] -- ^ parameters
     ,spsReturn :: !ReturnType
   }deriving (Show, Data, Typeable)

-- | A subprogram parameter.
data SubprogramParam
  = Param { 
      paramId   :: ![Identifier]
    , paramMode :: !Mode 
    , paramType :: SubtypeMark
    }
     deriving (Show, Data, Typeable)

data Mode
  = Omitted -- attention when parsing - some cases ommiting mode may 
            -- mean In whereas in other
            -- cases it has a different meaning (see own variables)
  | In
  | Out
  | InOut
   deriving (Show, Data, Typeable, Eq)

type ReturnType = Maybe SubtypeMark

-----------------------------------------------------------
-- ** Subprograms Annotations
--
-- Rule 6.1.1
-- Functions annotations and procedures annotations were grouped together
data SubprogramAnnot
  = SubprogramAnnot {
      spGlobals       :: Maybe GlobalDef          -- ^ global variables
    , spDependencies  :: Maybe DependencyRelation -- ^ (procedure) dependencies
    , spPrecondition  :: Maybe Exp
    , spPostcondition :: Maybe Postcondition
    }  deriving (Show, Data, Typeable)
  
-- DOUBT: I have joined the postcondition rule with return_annotation,
--        enabling us to keep handling procedures and functions with the
--        the same structures. note that a \predicate\ is a boolean expression.
data Postcondition
  = Post      Exp
  | Return    (Maybe Identifier) Exp
   deriving (Show, Data, Typeable)

type GlobalDef = [GlobalVar]

-- | Global Definition and Dependency Relations
--
-- Rule 6.1.2
--
data GlobalVar
  = GlobalVar !Mode
              [CompleteName]  -- DOUBT: Is this a 'CompleteName' ?
   deriving (Show, Data, Typeable)

data DependencyRelation
  = DependRel [DependencyClause]    -- ^ dependency clauses 
              [CompleteName]        -- ^ null dependency clause
   deriving (Show, Data, Typeable)

data DependencyClause
  = DependClause [CompleteName]
                 [CompleteName]     -- ^ imported vars
   deriving (Show, Data, Typeable)

-----------------------------------------------------------
-- ** Subprogram Body
--
-- Rule 6.3
--
data SubprogBody
  = SubprogramBody{
      spKind    :: !SubprogramKind          -- ^ Function or Procedure
    , spSpecs   :: SubprogramSpec           -- ^ specs
    , spAnnot   :: SubprogramAnnot          -- ^ annotations
    , spImpl    :: SubprogImpl              -- ^ implementation
    } deriving (Show, Data, Typeable)

data SubprogImpl
  = SubprogImplSS DeclarativePart           -- ^ declarative_part
                  Statements                -- ^ sequence_of_statements
  | SubprogImplCI                           -- [QualifiedExp] 
--                                 
--    The contents of the library package System.Machine_Code (if provided) are 
--    implementation defined. The meaning of code_statements is implementation 
--    defined. Typically, each qualified_expression represents a machine 
--    instruction or assembly directive.
--    (page 242, Ada reference)
   deriving (Show, Data, Typeable)

------------------------------------------------------------------------------
-- * Statements

-- Following Rule 5.1, 5.3, 5.4, 5.5, 5.7
type Statements = Statement

data Statement
  = NullStatement
  | HiddenStat      !PackageName !String  -- ^ caused by a "--# hide " annotation
  | AssignStat      {left::Name 
                    ,right::Exp} 
  | SubprogCallStat SubprogCall           -- ^ procedure_call_statement
  | ExitStat        (Maybe Identifier)    -- ^ exit statement name
                    (Maybe Exp)           -- ^ condition
  | ReturnStat      Exp
  | IfStat          Predicate
                    Statements                -- ^ then branch
                    [(Predicate, Statements)] -- ^ elsif
                    (Maybe Statements)        -- ^ else
  | CaseStat        Exp                             -- ^ condition
                    [([DiscreteChoice], Statements)] -- ^ case_statement_alternatives
                    (Maybe Statements)              -- ^ other
  | LoopStat        (Maybe Identifier)    -- ^ loop_statement_identifier
                    (Maybe ItScheme)      -- ^ iteration_scheme
                    (Maybe Predicate)     -- ^ Loop invariant
                    Statements            -- ^ sequence_of_statements
  | ProofStat       ProofKind             -- ^ Predicate may be an assert or a
                                          -- ^ check - Barnes A1.2
                    Predicate             -- ^ Proof Statements
  | Block           [Statement]
   deriving (Show, Data, Typeable)

data ProofKind
  = Assert
  | Check
   deriving (Eq, Show, Data, Typeable)

data NamedParam
  = NamedParam Identifier         -- ^ formarl_parameter_selector_name
               Exp 
   deriving (Eq, Show, Data, Typeable)

-- |SPARK Loop Statement
--
-- Rule 5.5
data ItScheme
  = While Exp                           -- ^ condition
  | For   Identifier                    -- ^ defining_identifier / var identifier
          Bool                          -- ^ if reverse is presented
          SubtypeMark                   -- ^ discrete_subtype_mark
          (Maybe Range)                 -- ^ range
   deriving (Show, Data, Typeable)

------------------------------------------------------------------------------
-- * Expressions

type SimpleExp = Exp
type Predicate = Exp

data Exp
   = Prim         Primary
   | BoolApp      BoolOp [Exp]
   | ArithApp     ArithOp [Exp]
   | RelApp       RelOp [Exp]
   | RangeApp     RangeOp (Either Range SubtypeMark) Exp
   | Quantified   QuantOp QuantExp
    deriving (Eq, Show, Data, Typeable)
   
-- Barnes book, appendix A1.2
data QualifiedExp 
  = QualExp Name Exp
  | QualAgg Name Aggregate
   deriving (Eq, Show, Data, Typeable)

------------------------------------------------------------------------------
-- ** Operations

data BoolOp = Not | And | Or | Xor | AndThen | OrElse | Implies | Iff
   deriving (Eq, Show, Data, Typeable)

data ArithOp = Neg | Plus | Minus | Mult | Div | Exp | Abs | Cat | Mod | Rem
   deriving (Eq, Show, Data, Typeable)

data RelOp = Eq | Neq | Lt | Gt | Lte | Gte
   deriving (Eq, Show, Data, Typeable)
  
-- IsIn is another name for the "In" operator, ghc complains
-- about multiple "In" definitions (we have In as a mode too).
data RangeOp = IsIn | IsNotIn
   deriving (Eq, Show, Data, Typeable)

data QuantOp = ForAll | ForSome
   deriving (Eq, Show, Data, Typeable)
   
data QuantExp
  = QuantExp Identifier Name (Maybe Range) Predicate 
   deriving (Eq, Show, Data, Typeable)
  
------------------------------------------------------------------------------
-- ** Expression atoms, primary values.

data Primary 
  = PrimLit       Literal
  | PrimName      Name
  | PrimTypeConv  SubtypeMark Exp      -- (victor) there might be a confusion here.
  | PrimQualExp   QualifiedExp
   deriving (Eq, Show, Data, Typeable)

------------------------------------------------------------------------------
-- ** Literals

--  Rules 2.3, 2.4, 2.5, 2.6
--
-- > literal ::=
-- >   numeric_literal | char_literal | str_literal
-- > numeric_literal ::=
-- >   decimal_literal | based_literal
--
-- Note that a decimal literal can be a floating point number, or
-- a integer.
data Literal
  = LitInt   (NumLit Integer)
  | LitReal  (NumLit Double)
  | LitChar  Char
  | LitStr   String
   deriving (Eq, Show, Data, Typeable)

data NumLit a = NumLit 
  { nNum :: a
  , nRep :: String
  }
  deriving (Data, Typeable)
  
instance Show (NumLit a) where
  show = nRep
  
instance (Eq a) => Eq (NumLit a) where
  a == b = (nNum a) == (nNum b)
  

-- | Identifier
-- Rule 2.3
-- > identifier ::=
-- >   identifier_letter {[underline] letter_or_digit}
type Identifier = String
  -- TODO: one single type for regular identifiers and operator symbols

------------------------------------------------------------------------------
-- * Declarations

--  Rule 3.1, 3.2.1, 3.2.2
--
data BasicDecl
  = BTypeDecl    Identifier TypeDecl
  | BSubtypeDecl Identifier         -- ^ subtype declaration identifier 
                 SubtypeMark        -- ^ existing subtype name 
                 (Maybe Constraint) -- ^ constraint about the existing subtype 
  | BObjDecl     [Identifier]       -- ^ list of names
                 Bool               -- ^ true if constant
                 SubtypeMark        -- ^ type of object
                 (Maybe Exp)        -- ^ default value
  | BNumDecl     [Identifier]       -- ^ list of names
                 Exp                -- ^ value
   deriving (Eq, Show, Data, Typeable)
  
-- |SPARK Subtype Constraints
-- Rule 3.6.1
-- > idx_constraint ::= "(" subtype_mark {"," subtype_mark} ")"
--
-- Rule 3.7.1, from the Ada reference. Rule not found in SPARK ref.
--
-- > disc_constraint ::= "(" disc_association { "," disc_association } ")"
-- > disc_association::= [ id { "|" id } "=>" ] expression
--
data Constraint
  = CScalar    Range                     -- ^ static_range
  | CIndex     [SubtypeMark]             -- ^ index constraint
  -- I think that Discriminant Constraints are RavenSPARK, since I did not
  -- find them in the JB's book.
  -- CDisc      [DiscriminantConstraint]  -- ^ discriminant_constraint
   deriving (Eq, Show, Data, Typeable)

--data DiscriminantConstraint
--  = DiscriminantConstraint [Identifier] Exp
--   deriving (Eq, Show, Data, Typeable)

------------------------------------------------------------------------------
-- ** Type Declarations

-- Note that Ada allows a discriminant_part (sec 3.7 of AdaSpec) to 
-- be optional in any type declaration, SPARK does not. Therefore:
-- Missing RavenSpark Declarations 
-- Rule 3.2.1, 7.3
--
--
data TypeDecl
  = FullTypeDecl   TypeDef
  | TDeclPrivDecl  Bool        -- ^ True if tagged
                   Bool        -- ^ True if limited
  | TDeclExtDecl   SubtypeMark (Maybe Constraint)
   deriving (Eq, Show, Data, Typeable)

------------------------------------------------------------------------------
-- ** Declarative Part
--
-- Rules 3.11, 10.1.3
data DeclarativePart 
  = DeclarativePart [RenameDecl] [Decls]
  | HiddenDeclPart  !PackageName !String  -- ^ caused by a "--# hide " annotation
   deriving (Show, Data, Typeable)

-- DOUBT: There is one more imprecision. In the reference manual appendix 
-- section 3.11 they have "declarative_item ::= basic_declarative_item | body |
-- generic_function_instantiation". This "generic_function_instantiation" does
-- not exists anywhere else neither in page 38 from the same manual. Shall we
-- ignore it?
data Decls 
  = DeclBasicDeclItem BasicDeclItem                     -- ^ basic_declarative_item
  | DeclSubprogBody SubprogBody
  | DeclPackageBody PackageBody
  | DeclSubprogBodyStub SubprogramSpec SubprogramAnnot  -- ^ subprogram_body_stub
  | DeclSubPackBodyStub Identifier                      -- ^ package_body_stub
  | EmbeddedPackDecl PackageDecl [Either RenameDecl SubtypeMark]
  | ExternalSubprogDecl SubprogramDecl                  -- DOUBT: not sure if 
                                                        -- we can ignore pragma
   deriving (Show, Data, Typeable)

------------------------------------------------------------------------------
-- * Definitions

------------------------------------------------------------------------------
-- ** Type Definitions

-- Rule 3.2.1, 3.5.1, 3.5.4, 3.6, 3.8
--
-- Rule 3.5.6, 3.5.7 and 3.5.9
-- > real_type_def ::=
-- >     "digits" simple_exp [ "range" simple_exp ".." simple_exp ]
-- >   | "delta" simple_exp "range" simple_exp ".." simple_exp
data TypeDef
  = TEnumTypeDef      [Identifier]      -- ^ enumeration elements
  | TIntTypeDef       IntRangeDef       -- ^ signed_integer_type_definition
  -- |expression, optinal SPairRange (a real type can be floating or fixed-point)
  | TRealTypeDef      FloatOrFixed
                      SimpleExp 
                      (Maybe (SimpleExp, SimpleExp)) -- ^ Real Range Lower Bound
  | TArrayTypeDef     Bool              -- ^ True if constrained array
                      [SubtypeMark]     -- ^ list of indexes
                      SubtypeMark       -- ^ type of the elements
  -- |A record can be tagged, and contains a list of components.
  | TRecordTypeDef    Bool              -- ^ True if tagged, it means: can be extended.
                      [TypeRecordComp]  -- "tagged record null end record" <=> "tagged null record"
  -- Barnes book, pag. 103
  | TRecordExtDef     SubtypeMark       -- ^ type_mark, assuming type_mark == subtype_mark
                      [TypeRecordComp]  -- "tagged record null end record" <=> "tagged null record"
  | TDerivedTypeDef   SubtypeMark
                      (Maybe Range)
   deriving (Eq, Show, Data, Typeable)
  
-- | Integer Ranges
data IntRangeDef
  = RangePair SimpleExp SimpleExp
  | RangeMod  SimpleExp
   deriving (Eq, Show, Data, Typeable)
  
data FloatOrFixed = Float | Fixed
   deriving (Eq, Show, Data, Typeable)

-- |Record Components
data TypeRecordComp 
  = TypeRecordComp [Identifier]   -- ^ components
                   SubtypeMark    -- ^ type of components
   deriving (Eq, Show, Data, Typeable)
  
-- |SPARK Ranges
-- Rules 3.5, 4.1.4
data Range
  = RangeStatic  SimpleExp SimpleExp
  | RangeAttrib  Prefix (Maybe Exp)
   deriving (Eq, Show, Data, Typeable)

-- |SPARK Discrete Choice
-- Rule 3.8.1
data DiscreteChoice
  = DCStaticSimpleExp     SimpleExp               -- ^ static_simpleexpression
  | DCDiscRange           Range                   -- ^ discrete_range - static_range
  | DCDiscRangeSubTypeInd SubtypeMark             -- ^ discrete_range - discrete_subtype_indication
                          (Maybe Constraint)  
   deriving (Eq, Show, Data, Typeable)

------------------------------------------------------------------------------
-- * Names

-- Rule 4.1, Barnes 113, 4.1.1, 4.1.3, 4.1.4 (partial)
--
data Name
  = DirectName    Identifier              -- ^ direct_name
  | SelectedComp  Prefix Identifier       -- ^ selected_component
  | AttrRef       Prefix AttrDesignator   -- ^ attribute_reference
  | FuncCall      SubprogCall             -- ^ function_call
  | DNameTilde    Identifier              -- ^ direct_name ~ (JB 393)   
  | SelCompTilde  Prefix Identifier       -- ^ selected_component ~ (JB 393)  
  | TypeConv      TypeConverssion         -- ^ type_converssion (JB 393)
  | RecordUpdate  Prefix [(Identifier, Exp)]    -- ^ record_update (JB 393)
  | ArrayUpdate   Prefix [([Exp], Exp)]   -- ^ array_update (JB 393)
   deriving (Eq, Show, Data, Typeable)
  
-- (victor) Suggestion:
--data Name
--  = DirectName      Identifier
--  
--  | AttrRef         Prefix AttrDesignator
--   deriving (Eq, Show, Data, Typeable)
--  
--data AnnotationName 
--  = SparkName       Name
--  | SparkNameTilde  Name
--  | RecordUpdate    Prefix [(Name, Exp)]
--  | ArrayUpdate     Prefix [(Exp,  Exp)]
--   deriving (Eq, Show, Data, Typeable)
--  
--data LValue
--  = SimpleName      Name
--  | AttrFunc        SubprogCall
--  | IndexedComp     Prefix [Exp]
--  | SelectedComp    Prefix Identifier
--   deriving (Eq, Show, Data, Typeable)
--
-- and we would change the following constructors:
-- Names inside annotations would be annotation names,
-- AssignStat would have an LValue, the rest stays the same.
  
type SubtypeMark = Name -- It may be helpful to understand

type Prefix = Name

data TypeConverssion
  = TypeConvName SubtypeMark Name
  | TypeConvExp  SubtypeMark Exp
   deriving (Eq, Show, Data, Typeable)

-- | Attribute Designator
data AttrDesignator
  -- DOUBT: Should we define all possible attributes?
  -- SUGGESTION: place these constructors in Name
  = ADesigIden Identifier [Exp] -- ^ where 0 < (length [Exp]) <= 2
  | Delta
  | Digits
   deriving (Eq, Show, Data, Typeable)

------------------------------------------------------------------------------
-- * Aggregate

-- DOUBT: positional array aggregates and positional record aggregates are
--        redundant. a type-checker is needed to decide wich one to choose.
-- DOUBT: Aggregates are always qualified (Barnes, page 110, third paragraph)
--        should we incorporate a "subtype_mark" with aggregates? this way we
--        would have one constructor less in the final tree.

-- Barnes pag. 109, RM 4.3
data Aggregate 
  = AggregatePos    [Exp]             -- ^ list of items
                    (Maybe Exp)       -- ^ "others =>", when applicable.
  | AggregateExt    Exp                         -- ^ ancestor_part
                    (Maybe [(Identifier, Exp)])
  | AggregateNamed  [NamedArrayComponent]
                    (Maybe Exp)
   deriving (Eq, Show, Data, Typeable)

data NamedArrayComponent
  = NamedArrayComponent [DiscreteChoice] Exp
   deriving (Eq, Show, Data, Typeable)

