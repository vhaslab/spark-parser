module Spark.Language.Parser where

import Data.Char(isUpper, digitToInt, toLower, ord, isDigit, isAlpha, isSpace)
import Data.List(isPrefixOf, splitAt)
import Control.Monad
import Control.Applicative hiding (many, (<|>), optional)
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Pos
import Text.ParserCombinators.Parsec.Prim
import Text.ParserCombinators.Parsec.Error
import Text.ParserCombinators.Parsec.Char(letter, digit, hexDigit)
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import Text.Parsec.Prim(ParsecT)
import System.FilePath

import qualified Spark.Language.AST as S
import qualified Spark.Language.Lexer as Token
import Spark.Language.Printer
import Spark.Language.ParserBinops

lexer 		  = Token.makeTokenParser

identifier 	= Token.identifier lexer
iden        = identifier

reservedOp  = Token.reservedOp lexer
reOp        = reservedOp

reserved    = Token.reserved lexer
re          = reserved

parens	    = Token.parens lexer
whitespace	= Token.whiteSpace lexer
integer     = Token.integer lexer 
comma		    = Token.comma lexer
colon		    = Token.colon lexer
dot			    = Token.dot lexer
brackets	  = Token.brackets lexer
angles 		  = Token.angles lexer
charLit		  = Token.charLiteral lexer
strLit		  = Token.stringLiteral lexer
semi        = Token.semi lexer
decimal     = Token.decimal lexer

-- |Parser p `followedBy` f succeds iff p and f succeds, returns p value.
followedBy :: Parser a -> Parser b -> Parser a
followedBy p flw = try p >>= (\x -> flw >> return x)

-- |Parses a sequence of keywords, returning nil.
wordSeq :: [String] -> Parser ()
wordSeq l = foldr (\h r -> re h >> r) (return ()) l

-- |Parses a keyword representing a value.
reTok       :: String -> a -> Parser a
reTok s v   = re s >> return v

-- |Returns True or False, representing the state of p.
optionBool :: Parser a -> Parser Bool
optionBool p = option False (p >> return True)

-- |Ignores the input until parser p succeeds, returns the ignored string.
ignoreUntil :: Parser a -> Parser String
ignoreUntil p =   (lookAhead p >> return "")
              <|> ((:) <$> anyChar <*> ignoreUntil p)


pair :: a -> b -> (a,b)
pair a b = (a,b)

main1 p f = do
  { ps <- parseFromFile (whitespace >> p) f
  ; case ps of
      Left e -> putStrLn (show e)
      Right ast -> putStrLn (show (pretty ast))
  }
  
main2 p f = do
  { ps <- parseFromFile p f
  ; case ps of
      Left e -> putStrLn (show e)
      Right ast -> putStrLn (show ast)
  }

main3 p f = do
  { ps <- parseFromFile (whitespace >> p) f
  ; case ps of
      Left e -> putStrLn (show e)
      Right ast -> (writeFile ("test"++(snd.splitExtension) f) (show $ pretty ast))
                   >> putStrLn (show (pretty ast))
  }
  
annotTest f = do
  { x <- readFile f
  ; (onc, offc) <- return (getContext x)
  ; putStrLn "on context: "
  ; putStr "\t"
  ; putStr onc
  ; putStr "\n\nrest:"
  ; putStr offc
  ; putStrLn "#"
  }
  
------------------------------------------------------------------------------
-- * Usefull functions

  
------------------------------------------------------------------------------
-- * Compilation Units

pLibraryItem :: Parser S.LibraryItem
pLibraryItem
  =   try (S.LibItemMainSubprog <$> pInheritAnnotation <*> (mainprog >> pSubprogramBody))                        
  <|> try (S.LibItemPackageBody <$> pPackageBody) 
  <|> (S.LibItemPackageDecl <$> pPackageDecl)
  where
    mainprog = pAnnotation "main_program" (return ())
    
pContext :: Parser S.Context
pContext = endBy pContextItem semi
    
pContextItem :: Parser S.ContextItem
pContextItem
  =   try (S.ContextItemWith <$> (re "with" >> (sepBy pPackageName comma)))
  <|> (S.ContextItemUse <$> (re "use" >> re "type" >> (sepBy pSubtypeMark comma)))
  
-- TODO: add subunits
pUnit :: Parser S.Unit
pUnit = S.UnitLibrary <$> pContext <*> pLibraryItem


------------------------------------------------------------------------------
-- * Annotation Contexts

-- TODO: in token "--#", the character '#' can be changed, for example, to a '%'
--       making the annotation token become "--%". The examiner supports that, should
--       we support it too?

onContextChar :: Char -> Parser a -> Parser a
onContextChar c p = do
  { str <- getInput
  ; prepos <- getPosition
  ; (onc, offc) <- return (getContextChar c str)
  ; setInput offc
  ; whitespace
  ; case (runParser p () "" onc) of
      (Left err) -> adjustPos onc >> fail (adjustError err prepos)
      (Right re) -> adjustPos onc >> return re
  }
  where
    count c = length . (filter (== c))
    
    adjustPos onc = do
      { p <- getPosition
      ; setPosition (setSourceLine p ((sourceLine p) + (count '\n' onc)))
      }
      
    errshow err = showErrorMessages 
                    "or" "unknown parse error" 
                    "expecting" "unexpected" "end of input"
                    (errorMessages err)
    
    adjustError err p = let
      errpos  = setSourceLine p (sourceLine (errorPos err) + sourceLine p - 1)
      errpos' = setSourceColumn errpos (sourceColumn (errorPos err) + sourceColumn p)
      errstr  = errshow err
      errstr' = "probable cause:\nannotation: " ++ (show errpos') ++ errstr
      in foldr (\c str ->
                case c of
                  '\n' -> '\n':'\t':str
                  _    -> c:str
                ) "" errstr'
        
      
  
getContextChar :: Char -> String -> (String, String)
getContextChar c s = let
  (onc, offc) = splitAt (walk c s) s
  onc' = rmvContext c onc
  in (onc', offc)
  where
    rmvContext c [] = []
    rmvContext c ('-':'-':d:l) | c == d = rmvContext c l
    rmvContext c ('-':'-':l) =  rmvContext c (dropWhile (/= '\n') l)
    rmvContext c (h:l) = h : (rmvContext c l)
    
    walk c s                      = walk' c False 0 s
    -- walk' commentFlag Acumulator String
    walk' c _     n []              = n
    walk' c False n (';':l)         = if hasMore c l then walk' c False (n+1) l else n + 1
    walk' c False n ('-':'-':d:l) | c == d = walk' c False (n+3) l
    walk' c False n ('-':'-':l)     = walk' c True (n+2) l
    walk' c True  n ('\n':l)        = walk' c False (n+1) l
    walk' c b     n (_:l)           = walk' c b (n+1) l
    
    hasMore c ('-':'-':d:p) | c == d = not (Token.isReservedAnnot (nextWord (dropWhile isSpace p)))
    hasMore c (h:t) | isSpace h = hasMore c t
                    | otherwise = False
    hasMore _ _ = False

    nextWord = getWhile [] isAlpha

    getWhile acu f [] = reverse acu
    getWhile acu f (h:t) | f h = getWhile (h:acu) f t
                         | otherwise = reverse acu
                      
onContext = onContextChar '#'
getContext = getContextChar '#'

pAnnotationStart :: Char -> Parser ()
pAnnotationStart c = reOp ("--" ++ [c])
    
pAnnotation, pAnnotationNoSemi :: String -> Parser a -> Parser a
pAnnotation s p = (try (pAnnotationStart '#' >> re s)
                >> onContext (p `followedBy` semi)) <?> (s ++ " annotation")
                
pAnnotationNoSemi s p = (pAnnotationStart '#' >> re s)
                      >> onContext p <?> (s ++ " annotation")

pMaybeAnnotation :: a -> String -> Parser a -> Parser a
pMaybeAnnotation ret s p = option ret (pAnnotation s p)

pHiddenAnnotation :: Parser (S.PackageName, String)
pHiddenAnnotation = do
  { n <- pAnnotationNoSemi "hide" pPackageName
  ; optional semi
  ; pair n <$> ignoreUntil (try (re "end" >> p n))
  }
  where
    p (S.PkgName Nothing id) = string id
    p (S.PkgName (Just par) id) = (p par) >> dot >> string id
    
-- |Instrumentation is used as a interface to our model checker and BMC.
pAnnotInstr :: String -> Parser a -> Parser a
pAnnotInstr s p = (try (pAnnotationStart '%' >> re s)
                       >> onContextChar '%' (p `followedBy` semi)) <?> "instrumentation"

------------------------------------------------------------------------------
-- * Packages

pPackageDecl :: Parser S.PackageDecl
pPackageDecl = (S.PackageDecl 
              <$> pInheritAnnotation
              <*> optionBool (re "private")
              <*> (re "package" >> pPackageName)
              <*> pPackageAnnotations
              <*> (re "is" >> pPackageSpec)
              <*> optionMaybe (re "private" >> pPackageSpec))
              `followedBy` (re "end" >> pPackageName >> semi)
    

pPackageName :: Parser S.PackageName
pPackageName = sepBy1 identifier dot >>= return . aux
  where
    aux = takeJust . (foldl (\a h -> Just (S.PkgName a h)) Nothing)
    takeJust (Just x) = x
    
pPackageSpec :: Parser S.PackageSpecPart
pPackageSpec
  = try (uncurry S.HiddenSpecPart <$> pHiddenAnnotation) 
  <|> (S.PackageSpecPart <$> pOptinalRenamingDecls <*> many pPackageDeclItem)       
  
pPackageBody :: Parser S.PackageBody
pPackageBody = ( S.PackageBody 
                  <$> (re "package" >> re "body" >> pPackageName)
                  <*> pRefinementDef
                  <*> (re "is" >> pDeclarativePart)
                  <*> option
                        (S.Block [])
                        (re "begin" >> pStatementSeq))
                `followedBy` (re "end" >> pPackageName >> semi)

-----------------------------------------------------------------------------
-- ** Package Annotations

pRefinementDef :: Parser [S.RefinementClause]
pRefinementDef = option [] (pAnnotation "own" (sepBy refs (reOp "&")))
  where
    refs = S.RefinementClause 
          <$> identifier
          <*> (re "is" >> clist)
    clist = sepBy (pair <$> pMode <*> pPackageName) comma
            
pOwnVar :: Parser S.OwnVar
pOwnVar = S.OwnVar <$> vars
                   <*> optionMaybe (reOp ":" >> pSubtypeMark)
        where
          vars = sepBy (pair <$> pMode <*> identifier) comma
  
pPackageAnnotations :: Parser S.PackageAnnotation
pPackageAnnotations
  = S.PackageAnnotation
    <$> pMaybeAnnotation [] "own" (sepBy pOwnVar comma)
    <*> pMaybeAnnotation [] "initializes" (sepBy pOwnVar comma)
    
pInheritAnnotation :: Parser [S.PackageName]
pInheritAnnotation = pMaybeAnnotation [] "inherit" (sepBy pPackageName comma)
    
-----------------------------------------------------------------------------
-- * Declarations

pOptinalRenamingDecls :: Parser [S.RenameDecl]
pOptinalRenamingDecls = option [] (try (endBy pRenamingDecl semi))

pDeclarativePart :: Parser S.DeclarativePart
pDeclarativePart = try (uncurry S.HiddenDeclPart <$> pHiddenAnnotation)
                 <|> (S.DeclarativePart <$> pOptinalRenamingDecls <*> many pDecls)

pDecls :: Parser S.Decls
pDecls
  =   try (S.DeclBasicDeclItem <$> pBasicDeclItem)
  <|> try (S.DeclSubprogBody <$> pSubprogramBody)
  <|> try (S.DeclPackageBody <$> pPackageBody)
  <|> try (S.DeclSubprogBodyStub 
            <$> pSubprogramSpec 
            <*> isSeparate pSubprogramAnnot)
  <|> try (S.DeclSubPackBodyStub <$> isSeparate (re "package" >> re "body" >> identifier))
  <|> try (S.ExternalSubprogDecl <$> havePragma pSubprogramDecl)
  where
    isSeparate = (`followedBy` (re "is" >> re "separate"))
    havePragma = (`followedBy` (re "pragma" >> re "Import" >> parens pragmaArgs))
    pragmaArgs = skipMany (satisfy (not . (`elem` "()"))) -- <|> parens pragmaArgs
--  | EmbeddedPackDecl PackageDecl [Either RenameDecl SubtypeMark]

-- test ok

pRenamingDecl :: Parser S.RenameDecl
pRenamingDecl
  =   S.PackageRename <$> (try (re "package") >> pPackageName)
                      <*> (re "renames" >> pPackageName)
  <|> S.SubprogRename <$> (func_or_proc >> pSubprogramSpec)
                      <*> (re "renames" >> pPackageName)
  where
    func_or_proc =  try (re "function")
                <|> (re "procedure")

pPackageDeclItem :: Parser S.PackageDeclItem
pPackageDeclItem
  =   (S.PackDeclItemBD  <$> pBasicDeclItem)
  <|> (S.PackDeclItemSD  <$> pSubprogramDecl)
  <|> (S.PackDeclItemESD <$> pSubprogramDecl)
  
pBasicDeclItem :: Parser S.BasicDeclItem
pBasicDeclItem
  =   try (S.ProofFunc <$> pAnnotation "function" pSubprogramSpec)
  <|> (S.ProofDecl <$> pProofDecl)
  <|> (S.BasicDeclItem <$> pBasicDecl)
  
pProofDecl :: Parser S.ProofDeclaration
pProofDecl
  =   (pAnnotation "type" 
        (S.ProofTypeDecl <$> identifier `followedBy` (re "is" >> re "abstract"))
      )
  <|> (pAnnotation "assert" passert)
  where
    passert = S.ProofTypeAssert
            <$> identifier `followedBy` (reOp "'" >> re "Base")
            <*> (re "is" >> pSubtypeMark)
            
pSubprogramDecl :: Parser S.SubprogramDecl
pSubprogramDecl = S.SubprogramDecl
                  <$> pSubprogramKind
                  <*> pSubprogramSpec `followedBy` semi
                  <*> pSubprogramAnnot
                where
                  pSubprogramKind =   (reTok "function" S.Function)
                                  <|> (reTok "procedure" S.Procedure)

pBasicDecl :: Parser S.BasicDecl
pBasicDecl = ((pTypeDecl
              <|> pSubtypeDecl
              <|> try pObjDecl
              <|> pNumDecl) `followedBy` semi) <?> "Basic Declaration"
  
pTypeDecl :: Parser S.BasicDecl
pTypeDecl = S.BTypeDecl <$> (re "type" >> identifier) 
                        <*> (re "is" >> innerTypeDecl)

innerTypeDecl :: Parser S.TypeDecl
innerTypeDecl
  =   try (S.FullTypeDecl <$> pTypeDef)
  <|> try (S.TDeclExtDecl <$> (re "new" >> pSubtypeMark) 
                          <*> (optionMaybe pConstraint) `followedBy` 
                                  (wordSeq ["with", "private"]) )
  <|> (S.TDeclPrivDecl <$> optionBool (re "tagged") 
                       <*> (optionBool (re "limited")) `followedBy` (re "private"))
 
    
pSubtypeDecl :: Parser S.BasicDecl
pSubtypeDecl = S.BSubtypeDecl 
             <$> (re "subtype" >> identifier)
             <*> (re "is" >> pSubtypeMark)
             <*> optionMaybe pConstraint
    
pObjDecl :: Parser S.BasicDecl
pObjDecl = S.BObjDecl
         <$> (sepBy identifier comma) `followedBy` (reOp ":")
         <*> optionBool (re "constant")
         <*> pSubtypeMark
         <*> optionMaybe (reOp ":=" >> pExp)
            
pNumDecl :: Parser S.BasicDecl
pNumDecl = S.BNumDecl 
         <$> (sepBy identifier comma) `followedBy` (reOp ":")
         <*> (re "constant" >> reOp ":=" >> pExp)

-----------------------------------------------------------------------------
-- * Type Definitions

pTypeDef :: Parser S.TypeDef
pTypeDef
  =   (try (S.TEnumTypeDef <$> parens (sepBy identifier comma))
  <|> try (S.TIntTypeDef <$> (S.RangePair <$> (re "range" >> pExp) 
                                          <*> (reOp ".." >> pExp)))
  <|> try (S.TIntTypeDef <$> (S.RangeMod  <$>  (re "mod" >> pExp)))
  <|> try pRealTypeDef
  <|> try pArrayTypeDef
  <|> try pRecordTypeDef
  <|> try pDerivedTypeDef
  <|> pRecordExt) <?> "Type Definition"
  
pRealRangeSpec :: Parser (Maybe (S.Exp, S.Exp))
pRealRangeSpec = optionMaybe (pair <$> (re "range" >> pExp)
                                   <*> (reOp ".."  >> pExp) )
  
pRealTypeDef :: Parser S.TypeDef
pRealTypeDef
  = try (S.TRealTypeDef S.Float <$> (re "digits" >> pExp)
                                <*> pRealRangeSpec)
  <|> (S.TRealTypeDef S.Fixed   <$> (re "delta" >> pExp)
                                <*> pRealRangeSpec)
                                
pArrayTypeDef :: Parser S.TypeDef
pArrayTypeDef = do
  re "array"
  (c, l) <- parens (try idx_st_l <|> disc_st_l)
  re "of"
  s <- pSubtypeMark
  return (S.TArrayTypeDef c l s)
  where
    idx_st_l = pair False <$> sepBy (pSubtypeMark 
                                    `followedBy` (re "range" >> reOp "<>")
                                    ) comma
    disc_st_l= pair True <$> sepBy pSubtypeMark comma
    
pRecordTypeDef :: Parser S.TypeDef
pRecordTypeDef
  = S.TRecordTypeDef <$> (option False (reTok "tagged" True))
                     <*> pRecordDef

pRecordExt :: Parser S.TypeDef
pRecordExt
  = S.TRecordExtDef <$> (re "new" >> pSubtypeMark)
                    <*> (re "with" >> pRecordDef)

pRecordComponent :: Parser S.TypeRecordComp
pRecordComponent
  = S.TypeRecordComp <$> (sepBy identifier comma) 
                     <*> (reOp ":" >> pSubtypeMark)

pRecCompList :: Parser [S.TypeRecordComp]
pRecCompList = endBy1 pRecordComponent semi

pRecordDef :: Parser [S.TypeRecordComp]
pRecordDef =   (wordSeq ["null", "record"] >> return [])
           <|> (re "record" >> 
                pRecCompList `followedBy` (re "end" >> re "record"))

pDerivedTypeDef :: Parser S.TypeDef
pDerivedTypeDef = S.TDerivedTypeDef <$> (re "new" >> pSubtypeMark)
                                    <*> optionMaybe (re "range" >> pRange)

-----------------------------------------------------------------------------
-- * Variables and Modes

pMode :: Parser S.Mode
pMode = (try (re "in") >> 
          ((reTok "out" S.InOut) <|> return S.In))
    <|> (try (reTok "out" S.Out))
    <|> return S.Omitted
    
-----------------------------------------------------------------------------
-- * Subprograms

-- Tested, working ok.
                    
pSubprogramSpec :: Parser S.SubprogramSpec
pSubprogramSpec
  =   S.SubprogramSpec
  <$> identifier
  <*> option [] (parens (pSubprogramParam `sepBy` semi))
  <*> pReturnType

pReturnType :: Parser S.ReturnType
pReturnType = optionMaybe (re "return" >> pSubtypeMark)

pSubprogramParam :: Parser S.SubprogramParam
pSubprogramParam = S.Param <$> sepBy identifier comma
                           <*> (reOp ":" >> pMode)
                           <*> pSubtypeMark
                           
pSubprogKind :: Parser S.SubprogramKind
pSubprogKind =   (reTok "function" S.Function)
             <|> (reTok "procedure" S.Procedure)                    
                           
pSubprogramBody :: Parser S.SubprogBody
pSubprogramBody = S.SubprogramBody
                  <$> pSubprogKind
                  <*> pSubprogramSpec
                  <*> pSubprogramAnnot
                  <*> (re "is" >> pSubprogImpl)
               
                  
pSubprogImpl :: Parser S.SubprogImpl
pSubprogImpl = S.SubprogImplSS
              <$> pDeclarativePart
              <*> between (re "begin") (re "end" >> pPackageName >> semi) pStatementSeq
                           
-----------------------------------------------------------------------------
-- ** Subprograms Annotations

pSubprogramAnnot :: Parser S.SubprogramAnnot
pSubprogramAnnot = S.SubprogramAnnot
                  <$> optionMaybe pGlobalDef
                  <*> optionMaybe pDependencies
                  <*> optionMaybe (pAnnotation "pre" pExp)
                  <*> optionMaybe pPostcondition
                  
pGlobalDef :: Parser [S.GlobalVar]
pGlobalDef = pAnnotation' "global" (many1 (try p))
  where 
    p = (S.GlobalVar <$> pMode <*> (sepBy pPackageName comma))
        `followedBy` semi
    pAnnotation' s p = try (reOp "--#" >> re s)
                      >> onContext p <?> (s ++ " annotation")

pDependencies :: Parser S.DependencyRelation
pDependencies = pAnnotation "derives" (pdeps <?> "dependency relation")
  where
    pdeps = S.DependRel <$> pDepClauses <*> ((optional (reOp "&") >> pNullDep) <|> return [])
    
pDepClauses :: Parser [S.DependencyClause]
pDepClauses = sepBy dclause (try (reOp "&" >> notFollowedBy (re "null")))
            <?> "dependency clause"
  where
    dclause = do
      { whitespace 
      ; vars <- sepBy pPackageName comma
      ; re "from"
      ; k <- optionBool (reOp "*")
      ; if k
          then do
            { char ','
            ; whitespace
            ; deps <- sepBy pPackageName comma
            ; return (S.DependClause vars (vars ++ deps))
            } <|> return (S.DependClause vars vars)
          else S.DependClause vars <$> sepBy pPackageName comma
      }
      
pNullDep :: Parser [S.CompleteName]
pNullDep = re "null" >> re "from" >> sepBy pPackageName comma

pPostcondition :: Parser S.Postcondition
pPostcondition =   pAnnotation "post"   (S.Post <$> pExp)
               <|> pAnnotation "return" pReturn 
               where
                pReturn = S.Return
                        <$> optionMaybe (try (identifier `followedBy` (reOp "=>")))
                        <*> pExp
                        
-----------------------------------------------------------------------------
-- * Names
                           
pSubtypeMark :: Parser S.Name
pSubtypeMark = do
  { nlist <- sepBy1 identifier dot
  ; case nlist of
      [n]   -> return (S.DirectName n)
      (h:t) -> return (foldl S.SelectedComp (S.DirectName h) t)
  } <?> "SubtypeMark"

pName :: Parser S.Name
pName = (pDirName >>= \n -> option n (try (pName1 n))) <?> "Name"

pName1 :: S.Name -> Parser S.Name
pName1 n = (pPrefixedName n) >>= \k -> option k (try (pName1 k))

pPrefixedName :: S.Name -> Parser S.Name
pPrefixedName n = pAttrRef n
              <|> pRecUpdate n
              <|> pArrayUpdate n
              <|> pSelectedComp n
              <|> pFuncCall n
              <|> pTypeConv n
  
pDirName :: Parser S.Name
pDirName = do
  { i <- identifier
  ; tilde <- optionBool (reOp "~")
  ; if tilde 
      then return (S.DNameTilde i)
      else return (S.DirectName i)
  }
  
pFuncCall :: S.Name -> Parser S.Name
pFuncCall n = S.FuncCall . (S.SubprogCall n) <$> pActualParms

pSelectedComp :: S.Name -> Parser S.Name
pSelectedComp n = do
  { dot
  ; i <- identifier
  ; tilde <- optionBool (reOp "~")
  ; if tilde
      then return (S.SelCompTilde n i)
      else return (S.SelectedComp n i)
  }

pTypeConv :: S.Name -> Parser S.Name
pTypeConv n = do 
  { t <- parens ((Left <$> pName) <|> (Right <$> pExp))
  ; case t of
      (Left m) -> return (S.TypeConvName n m)
      (Right e) -> return (S.TypeConvExp n e)
  } >>= return . S.TypeConv
  
  
pAttrRef :: S.Name -> Parser S.Name
pAttrRef n = reOp "'" >> (S.AttrRef n <$> pAttrDesignator)
  
pAttrDesignator :: Parser S.AttrDesignator
pAttrDesignator
  =   (reTok "Delta" S.Delta)
  <|> (reTok "Digits" S.Digits)
  <|> (S.ADesigIden <$> identifier <*> option [] (parens (sepBy pExp comma)))
  
pRecUpdate :: S.Name -> Parser S.Name
pRecUpdate n = (S.RecordUpdate n <$> brackets pRecUpdateList)

pRecUpdateList :: Parser [(S.Identifier, S.Exp)]
pRecUpdateList = sepBy (pair <$> identifier <*> (reOp "=>" >> pExp)) semi

pArrayUpdate :: S.Name -> Parser S.Name
pArrayUpdate n = (S.ArrayUpdate n <$> brackets pArrayUpdateList)

pArrayUpdateList :: Parser [([S.Exp], S.Exp)]
pArrayUpdateList = sepBy (pair <$> sepBy pExp comma <*> (reOp "=>" >> pExp)) semi
-----------------------------------------------------------------------------
-- * Statements
pBlock :: Parser a -> Parser b -> Parser b
pBlock d p = try (between d (re "end" >> d) p)

pStatement :: Parser S.Statement
pStatement
  =   ((reTok "null" S.NullStatement)
  <|> try (S.ExitStat <$> (re "exit" >> optionMaybe identifier)
                      <*> optionMaybe (re "when" >> pExp))
  <|> try (S.ReturnStat       <$> (re "return" >> pExp))
  <|> try assignOrCall
  <|> pBlock (re "if") (S.IfStat 
                          <$> pExp `followedBy` (re "then")
                          <*> pStatementSeq
                          <*> option [] pElseIf
                          <*> optionMaybe (re "else" >> pStatementSeq))
  <|> pBlock (re "case") (S.CaseStat <$> pExp `followedBy` (re "is")
                                      <*> many1 (try pCaseStatAlt)
                                      <*> optionMaybe pCaseStatOthers)
  <|> try ( do label <- optionMaybe (identifier `followedBy` (reOp ":"))
               itSc  <- optionMaybe pItScheme
               pAss  <- optionMaybe pAssertStat
               block <- (pBlock (re "loop") pStatementSeq) `followedBy` (optional identifier)
               return $ S.LoopStat label itSc pAss block))
  <?> "statement"
  
  --pLine >>= \x -> S.LoopStat 
  --          <$> optionMaybe (identifier `followedBy` (reOp ":"))
  --          <*> optionMaybe pItScheme
  --          <*> optionMaybe pAssertStat
  --          <*> (pBlock (re "loop") pStatementSeq) `followedBy` (optional identifier)
  --          <*> (return x)))
  --          <?> "statement"
  where
    assignOrCall = pName >>=
      \lval -> ((reOp ":=" >> pExp >>= \e -> return (S.AssignStat lval e))
                <|> (case lval of
                    (S.FuncCall s) -> return (S.SubprogCallStat s)
                    _              -> unexpected "Name"
                    )
                )
    pItScheme 
      =   (re "while" >> S.While <$> pExp)
      <|> (re "for" >> S.For 
                    <$> identifier
                    <*> (re "in" >> optionBool (re "reverse"))
                    <*> pSubtypeMark
                    <*> optionMaybe (re "range" >> pRange)
          )
    
    pElseIf = re "elsif" >> sepBy pElseIfBranch (re "elsif")
    pElseIfBranch = pair <$> pExp <*> (re "then" >> pStatementSeq)
    
    pCaseStatAlt = pair <$> (re "when" >> pDiscreteChoiceList) 
                        <*> (reOp "=>" >> pStatementSeq)
    pCaseStatOthers = (re "when" >> re "others" >> reOp "=>" >> pStatementSeq)
                
pProofStatement :: Parser S.Statement
pProofStatement =   (try (uncurry S.HiddenStat <$> pHiddenAnnotation) <?> "hidden annotation")
                <|> (try (S.ProofStat S.Check  <$> pCheckStat)        <?> "check annotation")
                <|> (try (S.ProofStat S.Assert <$> pAssertStat)       <?> "assert annotation")
               -- <|> (try (S.InstrAnnot <$> pAnnotInstr "notOverflow" overflow) <?> "instrumentation overflow")
               -- <|> ((S.InstrAnnot <$> pAnnotInstr "assert" pExp) <?> "instrumentation simple assert")
               -- <|> ((S.InstrAnnot <$> pInstr) <?> "instrumentation")


pStatementSeq :: Parser S.Statements
pStatementSeq = try (S.Block <$> many1 (pProofStatement <|> (pStatement `followedBy` semi)))

pSubprogCallStat :: Parser S.SubprogCall
pSubprogCallStat = S.SubprogCall <$> procname <*> pActualParms
  where
    procname = pName >>= \x -> notFollowedBy (reOp ":=") >> return x

pActualParms :: Parser S.ActualParameters
pActualParms = try (S.ExplicitParams <$> parens (sepBy pExp comma))
               <|> (S.NamedParams <$> parens (sepBy namedParam comma))
  where
    namedParam = S.NamedParam <$> identifier <*> (reOp "=>" >> pExp)
    
pAssertStat :: Parser S.Predicate
pAssertStat = pAnnotation "assert" pExp

pCheckStat :: Parser S.Predicate
pCheckStat = pAnnotation "check" pExp
                          
-----------------------------------------------------------------------------
-- * Aggregates

pAggregate :: Parser S.Aggregate
pAggregate =   try pPosAggregate
           <|> try pExtAggregate
           <|> pNamedAggregate
           
pPosAggregate = parens (S.AggregatePos 
                        <$> option [] (try (sepBy1 pExp comma))
                        <*> pOthers
                       )
                       
pOthers = optionMaybe (comma >> re "others" >> reOp "=>" >> pExp)

pExtAggregate = parens (S.AggregateExt <$> pExp <*> (re "with" >> pExtAggList))
  where
    pExtAggList
      = try (wordSeq ["null", "record"] >> return Nothing)
      <|> (Just <$> (sepBy pRecordCompAssoc comma))

    pRecordCompAssoc = pair <$> identifier <*> (reOp "=>" >> pExp)
    
pNamedAggregate = parens (S.AggregateNamed <$> option [] (sepBy1 pNamedArrayComponent comma)
                                           <*> pOthers)          
  
pNamedArrayComponent :: Parser S.NamedArrayComponent
pNamedArrayComponent
  = S.NamedArrayComponent <$> pDiscreteChoiceList <*> (reOp "=>" >> pExp)
  
-----------------------------------------------------------------------------
-- ** Discrete Choices

pDiscreteChoice :: Parser S.DiscreteChoice
pDiscreteChoice
  =   try (S.DCDiscRange <$> pRange)
  <|> try (S.DCDiscRangeSubTypeInd <$> pSubtypeMark <*> optionMaybe pConstraint)
  <|> (S.DCStaticSimpleExp <$> pExp)
  
pDiscreteChoiceList :: Parser [S.DiscreteChoice]
pDiscreteChoiceList = sepBy pDiscreteChoice (reOp "|")

-----------------------------------------------------------------------------
-- * Constraints
    
pConstraint :: Parser S.Constraint
pConstraint 
  =   (S.CScalar <$> (try (re "range") >> pRange))
  <|> (S.CIndex <$> parens (sepBy pSubtypeMark comma))
    
-----------------------------------------------------------------------------
-- * Expressions
      
pArithExp :: Parser S.Exp
pArithExp = buildExpressionParser arithOpList pExpAtom <?> "Arith Exp"

pRelOpExp :: Parser S.Exp
pRelOpExp = buildExpressionParser relOpList pArithExp <?> "Rel Exp"

pBoolExp :: Parser S.Exp
pBoolExp = buildExpressionParser boolOpList pRelOpExp <?> "Bool Exp"

pExp = pBoolExp

arithOpList = [
  [(Prefix (pArithPrefix S.Abs)),
   (Prefix (pBoolOp S.Not >> return ((S.BoolApp S.Not).(:[])) )),
   (Infix (pArithApp S.Exp)     AssocNone)], 

  [(Infix (pArithApp S.Mult)    AssocLeft),
   (Infix (pArithApp S.Div)     AssocLeft),
   (Infix (pArithApp S.Mod)     AssocLeft),
   (Infix (pArithApp S.Rem)     AssocLeft)],
   
  [(Prefix (pArithPrefix S.Neg)),
   (Infix (pArithApp S.Plus)    AssocLeft),
   (Infix (pArithApp S.Minus)   AssocLeft),
   (Infix (pArithApp S.Cat)     AssocLeft)]
  ]
  
relOpList = [
  [(Infix (pRelApp S.Eq)        AssocLeft)],
  [(Infix (pRelApp S.Neq)       AssocLeft),
   (Infix (pRelApp S.Lt)        AssocLeft),
   (Infix (pRelApp S.Lte)       AssocLeft),
   (Infix (pRelApp S.Gt)        AssocLeft),
   (Infix (pRelApp S.Gte)       AssocLeft)],
  [(Postfix (pRangeApp S.IsIn)),
   (Postfix (pRangeApp S.IsNotIn))]
  ]
     
boolOpList = [
  [(Infix (pBoolApp S.And)      AssocLeft),
   (Infix (pBoolApp S.Or)       AssocLeft),
   (Infix (pBoolApp S.Xor)      AssocLeft)],
  [(Infix (pBoolApp S.Implies)  AssocLeft),
   (Infix (pBoolApp S.Iff)      AssocLeft)]
  ]
    
-----------------------------------------------------------------------------
-- ** Expression Atom

pExpAtom :: Parser S.Exp
pExpAtom 
  =   (((S.Prim . S.PrimLit) <$> pLiteral)           <?> "Literal")
  <|> ((S.Quantified <$> pQuantOp <*> pQuantExp)     <?> "Quantification")
  <|> try (((S.Prim . S.PrimQualExp) <$> pQualifiedExp)  <?> "Qualified Exp")
  <|> (((S.Prim . S.PrimName) <$> pName)             <?> "Name")
  <|> parens pExp 
  
pQualification :: Parser S.SubtypeMark
pQualification = pSubtypeMark `followedBy` (reOp "'")
  
pQualifiedExp :: Parser S.QualifiedExp
pQualifiedExp =   try (S.QualAgg <$> pQualification <*> pAggregate)
              <|> (S.QualExp <$> pQualification <*> parens pExp)

-----------------------------------------------------------------------------
-- *** Range Operators

pRangeApp :: S.RangeOp -> Parser (S.Exp -> S.Exp)
pRangeApp x 
  | x == S.IsIn    = S.RangeApp S.IsIn <$> (re "in" >> rt)
  | x == S.IsNotIn = S.RangeApp S.IsNotIn <$> (re "not" >> re "in" >> rt)
  where
    rt = (Left <$> try pRange) <|> (Right <$> pSubtypeMark)

-----------------------------------------------------------------------------
-- *** Quantifications

pQuantOp :: Parser S.QuantOp
pQuantOp = re "for" >>  (try (reTok "all" S.ForAll)
                         <|> (reTok "some" S.ForSome) )
  
pQuantExp :: Parser S.QuantExp
pQuantExp 
  = S.QuantExp
  <$> identifier `followedBy` (re "in")
  <*> pSubtypeMark 
  <*> optionMaybe (re "range" >> pRange)
  <*> (reOp "=>" >> pExp)
  
-----------------------------------------------------------------------------
-- ** Ranges

pRange :: Parser S.Range
pRange
  =   try (S.RangeStatic <$> (pExp `followedBy` (reOp "..")) 
                         <*> pExp)
  <|> (S.RangeAttrib <$> pQualification 
                     <*> (re "Range" >> optionMaybe (parens pExp)))

-----------------------------------------------------------------------------
-- ** Literals

pLiteral :: Parser S.Literal
pLiteral = pLiteral' `followedBy` whitespace

pLiteral' :: Parser S.Literal
pLiteral'
  =   ((pNumLit   <$> pSparkNumeral) <?> "Number")
	<|> ((S.LitChar <$> chrLit)        <?> "Character")
	<|> ((S.LitStr  <$> strLit)        <?> "String")
	where
    chrLit = Token.charLiteral lexer
    strLit = Token.stringLiteral lexer
    
pNumLit :: [String] -> S.Literal
pNumLit l = if ('.' `elem` (head l)) 
            then S.LitReal (pNumRealLit l)
            else S.LitInt  (pNumIntLit l)
                    
    
pNumIntLit :: [String] -> S.NumLit Integer
pNumIntLit [n] = S.NumLit (read (filter (/= '_') n)) n
pNumIntLit [b, n] = pNumIntLit [b, n, "0"]
pNumIntLit [b, n, e] = 
  let
    n' = filter (/= '_') n
    rep = b ++ "#" ++ n ++ "#" ++ (if e == "0" then "" else e)
    (Just e') = (Just 10)
    b' = read b
  in S.NumLit ((toDec b' n') * b' ^ e') rep
  where
    toDec b n = toDec' b (map d2i n)
    toDec' b  = fst . (foldr (\h (r, c) -> (r+h*(b^c), c+1)) (0, 0))
    d2i       = toInteger . digitToInt
    
pNumRealLit :: [String] -> S.NumLit Double
pNumRealLit [n] = S.NumLit (read (filter (/= '_') n)) n

pSparkNumeral :: Parser [String]
pSparkNumeral =   try based
              <|> try (float >>= return . (:[])) 
              <|> (int >>= return . (:[]))
  where

    fdot = char '.' >> return "."

    sign = (char '+' >> return "+") <|> (char '-' >> return "-")
     
    int = many1 (oneOf "0123456789_")

    float = pApply [int, option "" (pApply [fdot, int]), option "" expn]

    expn = do
      { char 'E'
      ; s <- option "+" sign
      ; i <- int
      ; return ("E" ++ s ++ i)
      }
    
    based = do
      { base <- int
      ; char '#'
      ; n <- many1 (oneOf "ABCDEF0123456789_")
      ; char '#'
      ; e <- option "" expn
      ; return [base, n, e]
      }

    pApply l = pApply' l ""
    pApply' [] s = return s
    pApply' (h:t) s = h >>= \s' -> pApply' t (s ++ s')
