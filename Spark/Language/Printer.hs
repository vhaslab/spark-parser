{-# LANGUAGE FlexibleInstances #-}
module Spark.Language.Printer where

import Text.PrettyPrint as PP
import Spark.Language.AST as S

class Pretty a where
  pretty :: a -> Doc
  plvl   :: Int -> a -> Doc
  prettyPrefix :: Doc -> a -> Doc
  prettySufix  :: Doc -> a -> Doc
  
  pretty = plvl 0
  plvl n = (nest n) . pretty
  prettyPrefix p = (p <>) . pretty
  prettySufix  p = (<> p) . pretty
  
instance (Pretty a) => Pretty (Maybe a) where
  pretty (Just a)   = pretty a
  pretty (Nothing)  = empty 
  
  prettyPrefix p (Just a) = p <> pretty a
  prettyPrefix _ Nothing  = empty
  
  prettySufix  p (Just a) = pretty a <> p
  prettySufix  _ Nothing  = empty
  
instance (Pretty a, Pretty b) => Pretty (Either a b) where
  pretty (Left x)  = pretty x
  pretty (Right x) = pretty x
  
instance Pretty String where
  pretty = text
  
instance Pretty Doc where
  pretty = id

-- | Abreviations
t       = text
tif s b = if b then (t s) else empty

separateWith :: (Pretty a) => Doc -> [a] -> Doc
separateWith d l = hsep (punctuate d (map pretty l))

prettyPairList :: (Pretty a, Pretty b) => Doc -> Doc -> [(a, b)] -> Doc
prettyPairList isep psep l = 
  let
    f (x, y) = pretty x <+> psep <+> pretty y    
  in hsep (punctuate isep (map f l))
  
unpretty :: (Show a) => a -> Doc
unpretty x = t "%" <> t (show x) <> t "%"

unprettyPrefix :: (Show a) => Doc -> a -> Doc
unprettyPrefix p = (p <>) . unpretty

unprettySufix :: (Show a) => Doc -> a -> Doc
unprettySufix p = (<> p) . unpretty


------------------------------------------------------------------------------
-- * Compilation Units

instance Pretty Context where
  pretty l = vcat (map ((<> semi).pretty) l)
  
instance Pretty ContextItem where
  pretty (ContextItemWith l) = t "with" <+> separateWith comma l
  pretty (ContextItemUse l) = t "use" <+> t "type" <+> separateWith comma l
  
instance Pretty LibraryItem where
  pretty (LibItemPackageDecl p) = pretty p
  pretty (LibItemPackageBody p) = pretty p
  pretty (LibItemMainSubprog i p) = t "--# main_program;"
                                  $+$ pkgInherits i 
                                  $+$ pretty p
    where
      pkgInherits [] = empty
      pkgInherits l = prettyAnnot "inherit" (separateWith comma l)
      
instance Pretty Unit where
  pretty (UnitSubunit _ _) = t "%todo%"
  pretty (UnitLibrary c u) = pretty c $+$ pretty u

------------------------------------------------------------------------------
-- * Names

instance Pretty Name where
  pretty (DirectName n) = t n
  pretty (SelectedComp p i) = pretty p <> t "." <> t i
  pretty (AttrRef p a) = pretty p <> t "'" <> pretty a
  pretty (FuncCall spc) = pretty spc
  pretty (DNameTilde n) = t n <> t "~"
  pretty (SelCompTilde p i) = pretty (SelectedComp p i) <> t "~"
  pretty (TypeConv tconv) = pretty tconv
  pretty (RecordUpdate n l) = pretty n <> t "[" 
                                       <> prettyPairList semi (t "=>") l
                                       <> t "]"
  pretty (ArrayUpdate n l) =  pretty n <> t "[" 
                                       <> prettyPairList semi (t "=>") (map f l)
                                       <> t "]"
                            where
                              f (expl, a) = (separateWith comma expl, a)
  
  
instance Pretty AttrDesignator where
  pretty Delta = t "Delta"
  pretty Digits = t "Digits"
  pretty (ADesigIden i e) = t i <+> if e == []
                                      then empty
                                      else (parens . pretty) (head e)
                                      
instance Pretty TypeConverssion where
  pretty (TypeConvName n m) = pretty n <+> parens (pretty m)
  pretty (TypeConvExp n e) = pretty n <+> parens (pretty e)


------------------------------------------------------------------------------
-- * Annotations

prettyAnnot :: String -> Doc -> Doc
prettyAnnot s d = t "--#" <+> t s <+> d <> semi

------------------------------------------------------------------------------
-- * Packages

instance Pretty PackageName where
  pretty (PkgName Nothing i) = text i
  pretty (PkgName (Just p) i) = (pretty p) <> char '.' <> text i
  
instance Pretty PackageSpecPart where
  pretty (PackageSpecPart renames decls) 
    =  vcat (map ((<> semi) . pretty) renames) 
    $$ vcat (map ((<> char '\n').pretty) decls) 
  pretty (HiddenSpecPart pname str)
    =   prettyAnnot "hide" (pretty pname) <> char '\n'
    $+$ t str
    
instance Pretty RenameDecl where
  pretty (PackageRename n m) = (t "package") <+> (pretty n)
                                             <+> (t "renames")
                                             <+> (pretty m)
  pretty (SubprogRename s m) = (pretty s) <+> (t "renames")
                                          <+> (pretty m)
                                          
instance Pretty PackageDecl where
  pretty (PackageDecl childs priv name annot spec privspec)
    =   pkgInherits childs
    $$  tif "private" priv
    <+> t "package"
    <+> pretty name
    $+$ pretty annot
    $+$ t "is"
    $+$ (nest bNest (pretty spec))
    $+$ pkgPrivSpec privspec
    $+$ t "end" <+> pretty name <> semi
    where
      pkgInherits [] = empty
      pkgInherits l = prettyAnnot "inherit" (separateWith comma l)
      
      pkgPrivSpec Nothing = empty
      pkgPrivSpec (Just s)= t "private" $+$ nest bNest (pretty s)
                                          
instance Pretty PackageBody where
  pretty (PackageBody name ref decls sts)
    =   t "package"
    <+> t "body"
    <+> pretty name
    $$  pkgRefinement ref
    $+$ t "is"
    $+$ nest bNest (pretty decls)
    $+$ pkgSts sts
    $+$ t "end" <+> pretty name <> semi
    where
      pkgSts (Block []) = empty
      pkgSts l  = t "begin" $+$ (nest bNest (pretty l))
      
      pkgRefinement [] = empty
      pkgRefinement l = prettyAnnot "own" (separateWith (t " &") l)
      
instance Pretty RefinementClause where
  pretty (RefinementClause id cts) = t id <+> t "is" <+> f cts
    where
      f = hsep . (punctuate comma) . (map (\(m, ty) -> pretty m <+> pretty ty))
                                      
-----------------------------------------------------------------------------
-- ** Package Annotations

instance Pretty PackageAnnotation where
  pretty (PackageAnnotation owns inits)
    = (if length owns == 0
        then empty
        else prettyAnnot "own" owns')
    $+$ if length inits == 0
          then empty
          else prettyAnnot "initializes" inits'
    where
      owns' = separateWith comma owns
      inits' = separateWith comma inits

-----------------------------------------------------------------------------
-- * Declarations 

instance Pretty PackageDeclItem where
  pretty (PackDeclItemBD b) = pretty b
  pretty (PackDeclItemSD s) = pretty s
  pretty (PackDeclItemESD s) = pretty s
  

instance Pretty BasicDecl where
  pretty (BTypeDecl id def) = t "type" <+> t id <+> t "is" $$ nest bNest (pretty def)
  pretty (BSubtypeDecl id sup ct) = t "subtype" <+> t id
                                                <+> t "is"
                                                <+> pretty sup
                                                <+> pretty ct
  pretty (BObjDecl vars cons ty defexp)
    = separateWith comma vars
      <>  colon
      <+> tif "constant" cons
      <+> pretty ty
      <+> prettyPrefix (t ":= ") defexp
  pretty (BNumDecl vars exp) 
    = separateWith comma vars
      <> colon
      <+> t "constant" <+> t ":=" <+> pretty exp
      
instance Pretty Decls where
  pretty (DeclBasicDeclItem b) = pretty b
  pretty (DeclSubprogBody b) = pretty b
  pretty (DeclPackageBody b) = pretty b
  pretty (DeclSubprogBodyStub spec annot)
    = pretty spec <+> pretty annot <+> t "is" <+> t "separate"
  pretty (DeclSubPackBodyStub i) 
    = t "package" <+> t "body" <+> t i <+> t "is" <+> t "separate"
  pretty (EmbeddedPackDecl pdecl l)
    =   pretty pdecl
    $+$ vcat (map ((<> semi) . (either pretty f)) l)
    where
      f n = t "use" <+> t "type" <+> pretty n
  pretty (ExternalSubprogDecl spec)
    =   pretty spec 
    $+$ (t "pragma" <+> t "Import(...) -- ignored")
    
instance Pretty BasicDeclItem where
  pretty (BasicDeclItem b) = pretty b <> semi
  pretty (RepresentationClause) = t "-- representation clause, ignored"
  pretty (ProofFunc s) = prettyAnnot "function" (pretty s)
  pretty (ProofDecl d) = pretty d
  
instance Pretty ProofDeclaration where
  pretty (ProofTypeDecl i) = prettyAnnot "type" (t i <+> t "is" <+> t "abstract")
  pretty (ProofTypeAssert i ty) = prettyAnnot "assert" (t i <> t "'Base"
                                                        <+> t "is" <+> pretty ty)
  
instance Pretty DeclarativePart where
  pretty (DeclarativePart renames decls)
    =   vcat (map ((<> semi) . pretty) renames)
    $+$ vcat (map ((<> char '\n').pretty) decls)
  pretty (HiddenDeclPart pname str) 
    =   prettyAnnot "hide" (pretty pname) <> char '\n'
    $+$ t str
      
-----------------------------------------------------------------------------
-- ** Type Declarations 
      
instance Pretty TypeDecl where
  pretty (FullTypeDecl ftd) = pretty ftd
  pretty (TDeclExtDecl ty c)= t "new" 
                             <+> pretty ty
                             <+> pretty c
                             <+> t "with" <+> t "private"
  pretty (TDeclPrivDecl tagged limited) = tif "tagged" tagged
                                        <+> tif "limited" limited
                                        <+> t "private"
    

-----------------------------------------------------------------------------
-- * Type Definitions

prettyRTD :: Maybe (Exp, Exp) -> Doc
prettyRTD Nothing = empty
prettyRTD (Just (e1, e2)) = t "range" <+> pretty (RangeStatic e1 e2)

instance Pretty TypeDef where
  pretty (TEnumTypeDef names) = parens (separateWith comma names)
  pretty (TIntTypeDef p) = pretty p
  pretty (TRealTypeDef Float dig ran) = t "digits" <+> unpretty dig <+> prettyRTD ran
  pretty (TRealTypeDef Fixed dig ran) = t "delta"  <+> unpretty dig <+> prettyRTD ran
  pretty (TArrayTypeDef c l s)
    = t "array"
      <+> (if c
        then parens (separateWith comma l)
        else parens (hsep (punctuate 
                           comma 
                           (map (prettySufix (t " range" <+> t "<>")) l) )
                    ))
      <+> t "of"
      <+> pretty s
  pretty (TRecordTypeDef tagged rdef) = tif "tagged" tagged <+> pretty rdef
  pretty (TRecordExtDef  sup    rdef) = t "new" <+> pretty sup
                                                <+> t "with"
                                                <+> pretty rdef
  pretty (TDerivedTypeDef n ran) = t "new" <+> pretty n <+> prettyPrefix (t "range ") ran
                
                                                
instance Pretty TypeRecordComp where
  pretty (TypeRecordComp vars ty) = separateWith comma vars
                                  <+> colon
                                  <+> pretty ty
                                  <> semi
                                  
instance Pretty [TypeRecordComp] where
  pretty [] = t "null" <+> t "record"
  pretty l  = t "record" 
            $+$ nest bNest (vcat (map pretty l))
            $+$ t "end" <+> t "record"
            
instance Pretty IntRangeDef where
  pretty (RangePair e1 e2) = t "range" <+> pretty (RangeStatic e1 e2)
  pretty (RangeMod  e) = t "mod" <+> pretty e

-----------------------------------------------------------------------------
-- * Variables and Modes

instance Pretty S.Mode where
  pretty In       = t "in"
  pretty Out      = t "out"
  pretty InOut    = t "in out"
  pretty Omitted  = empty
  
instance Pretty OwnVar where
  pretty (OwnVar vars ty) = vars' <+> prettyPrefix (t " : ") ty
    where
      vars' = hsep (punctuate comma (map p vars))
      p (m, i) = pretty m <+> t i

-----------------------------------------------------------------------------
-- * Subprograms

instance Pretty SubprogramSpec where
  pretty (SubprogramSpec id parms Nothing)
    = (t "procedure") <+> (t id) 
                      <> if length parms == 0 
                            then empty 
                            else parens (separateWith semi parms)
  pretty (SubprogramSpec id parms (Just ret))
    = (t "function") <+> (t id) 
                     <> (if length parms == 0 
                            then empty 
                            else parens (separateWith semi parms))
                     <+> (t "return") <+> pretty ret
                                 
instance Pretty SubprogramParam where
  pretty (Param i m ty) = separateWith comma i <+> colon <+> pretty m <+> pretty ty
  
instance Pretty SubprogBody where
  pretty (SubprogramBody _ s a imp)
    =   pretty s
    $+$ (nest bNest (pretty a))
    $+$ t "is" 
    $+$ (nest bNest (pretty imp))
    $+$ t "end" <+> t (subprogname s) <> semi
    where
      subprogname (SubprogramSpec n _ _) = n
      
instance Pretty SubprogramDecl where
  pretty (SubprogramDecl _ spec annot)
    = pretty spec <> semi $+$ pretty annot
               
instance Pretty SubprogImpl where
  pretty (SubprogImplCI) = t "-- code insertion"
  pretty (SubprogImplSS d s)
    =   pretty d
    $+$ (nest (-bNest) (t "begin"))
    $+$ pretty s
    
instance Pretty SubprogCall where
  pretty (SubprogCall n parms) = pretty n <> pretty parms
  
instance Pretty ActualParameters where
  pretty (ExplicitParams l) = parens (separateWith comma l)
  pretty (NamedParams l) = parens (separateWith comma l)
  
instance Pretty NamedParam where
  pretty (NamedParam i e) = t i <+> t "=>" <+> pretty e
    
-----------------------------------------------------------------------------
-- * Subprogram Annotations

instance Pretty SubprogramAnnot where
  pretty (SubprogramAnnot globs deps pre post)
    =   pretty globs
    $+$ pretty deps
    $+$ f pre
    $+$ pretty post
    where
      f Nothing = empty
      f x = t "--#" <+> t "pre" <+> pretty x <> semi
  
instance Pretty GlobalVar where
  pretty (GlobalVar mode name) = pretty mode <+> separateWith comma name
  
instance Pretty [GlobalVar] where
  pretty l = prettyAnnot "global" (separateWith semi (map pretty l))

instance Pretty DependencyRelation where
  pretty (DependRel cs nc) =
    let
      contextNest = (t "--#" $$) . (nest (6*bNest))
      
      clauses = if (nc == [])
                  then cs
                  else cs ++ [DependClause [(PkgName Nothing "null")] nc]
                  
      pclauses = map pretty clauses
    in if length clauses == 0 
      then empty 
      else  t "--#" <+> t "derives" 
            $$ vcat (map contextNest (punctuate (t " &") pclauses))
            <>  semi
      
  
instance Pretty DependencyClause where
  pretty (DependClause vars froms) = 
    let
      docdeps _ [] = []
      docdeps True (h:hs)  = if h `elem` vars 
                              then docdeps True hs 
                              else (pretty h):(docdeps True hs)
      docdeps False (h:hs) = if h `elem` vars 
                              then (t "*") : (docdeps True hs)
                              else (pretty h):(docdeps False hs)
    in vars' <+> t "from" <+> separateWith comma froms -- (docdeps False froms)
    where
      vars' = separateWith comma vars

instance Pretty Postcondition where
  pretty (Post exp) = prettyAnnot "post" (pretty exp)
  pretty (Return i e) = prettyAnnot 
                          "return" 
                          (prettySufix (t " =>") i <+> pretty e)

-----------------------------------------------------------------------------
-- * Statements

bNest = 2

prettyBlock :: Doc -> Doc -> Doc -> Doc
prettyBlock b e x = b $+$ (nest bNest x) $+$ e

instance Pretty Statement where
  pretty (HiddenStat pname str) = prettyAnnot "hide" (pretty pname) <> char '\n' $+$ t str
  pretty (NullStatement) = t "null" <> semi
  pretty (ExitStat id c) = t "exit" <+> pretty id
                                    <+> prettyPrefix (t "when ") c 
                                    <> semi
  pretty (ReturnStat e) = t "return" <+> pretty e <> semi
  pretty (AssignStat i e) = pretty i <+> t ":=" <+> pretty e <> semi
  pretty (SubprogCallStat p) = pretty p <> semi
  pretty (IfStat p s l e) 
    = (prettyBlock (t "if" <+> pretty p <+> t "then") (t "end" <+> t "if")
      (   pretty s
      $+$ prettyElsif l
      $+$ prettyElse  e
      )) <> semi
      where
        prettyElsif [] = empty
        prettyElsif ((p, s):l) = (nest (-bNest) (t "elsif" <+> pretty p <+> t "then"))
                               $+$ pretty s
                               $+$ prettyElsif l
        prettyElse Nothing = empty
        prettyElse (Just s)= (nest (-bNest) (t "else")) $+$ pretty s
  pretty (CaseStat e l o) 
    = prettyBlock (t "case" <+> pretty e <+> t "is") 
                  (t "end" <+> t "case")
                  (prettyCaseAlts l $+$ prettyOthers o) 
      <> semi
      where
        prettyCaseAlts [] = empty
        prettyCaseAlts ((c, s):l)
          =   (t "when" <+> pretty c <+> t "=>")
          $+$ (nest (2*bNest) (pretty s))
          $+$ prettyCaseAlts l
        prettyOthers Nothing = empty
        prettyOthers (Just s)= (t "when" <+> t "others" <+> t "=>") 
                               $+$ (nest (2*bNest) (pretty s))
  pretty (LoopStat i s p ss) 
    = prettySufix colon i
    <+> pretty s
    $+$ (if isNotNothing s
          then prettyAnnot "assert" (pretty p)
          else empty)
    $+$ prettyBlock (t "loop") (t "end" <+> t "loop" <+> pretty i) (pretty ss) 
    <> semi
    where
      isNotNothing Nothing = False
      isNotNothing _       = True
  pretty (ProofStat Check p) = prettyAnnot "check" (pretty p)
  pretty (ProofStat Assert p)= prettyAnnot "assert" (pretty p)
  pretty (Block sts) = vcat (map pretty sts)

instance Pretty [Statement] where
  pretty = vcat.(map pretty)

instance Pretty ItScheme where
  pretty (While e) = t "while" <+> pretty e
  pretty (For i rev ty r) = t "for" <+> pretty i
                                   <+> t "in"
                                   <+> tif "reverse" rev
                                   <+> pretty ty
                                   <+> prettyPrefix (t "range ") r


-----------------------------------------------------------------------------
-- * Aggregates

instance Pretty Aggregate where
  pretty (AggregatePos p o) = parens (separateWith comma p 
                                      <+> prettyPrefix (t ", others =>") o
                                     )
  pretty (AggregateExt anc r) = parens (pretty anc <+> t "with" <+> prettyExt r)
    where
      prettyExt Nothing = t "null" <+> t "record"
      prettyExt (Just r)= hsep (punctuate comma (map prettyRecField r))
      
      prettyRecField (i, e) = pretty i <+> t "=>" <+> pretty e
  
  pretty (AggregateNamed l o) = parens (separateWith comma l
                                        <+> prettyPrefix (t ", others =>") o
                                       )
                                       
instance Pretty NamedArrayComponent where
  pretty (NamedArrayComponent dcl aggitem) = pretty dcl <+> t "=>" <+> pretty aggitem

-----------------------------------------------------------------------------
-- ** Discrete Choices

instance Pretty DiscreteChoice where
  pretty (DCDiscRange r) = pretty r
  pretty (DCDiscRangeSubTypeInd n c) = pretty n <+> pretty c
  pretty (DCStaticSimpleExp e) = pretty e
  
instance Pretty [DiscreteChoice] where
  pretty l = hcat (punctuate (t "|") (map pretty l))

-----------------------------------------------------------------------------
-- * Constraints

instance Pretty Constraint where
  pretty (CScalar r) = t "range" <+> pretty r
  pretty (CIndex  i) = parens (separateWith comma i)

-----------------------------------------------------------------------------
-- * Expressions

bopIdx :: BoolOp -> Int
bopIdx x | x `elem` [And, Or, Xor, AndThen, OrElse, Implies, Iff] = 1
         | otherwise = 2

aopIdx :: ArithOp -> Int
aopIdx x | x `elem` [Plus, Minus, Cat] = 3
         | x `elem` [Mult, Div, Mod, Rem] = 4
         | x `elem` [Exp, Abs, Neg] = 5
         
ropIdx :: RelOp -> Int
ropIdx x | x == Eq = 2
         | otherwise = 3
         
prettyOp :: Doc -> [Doc] -> Doc
prettyOp op args = hcat (punctuate op args) 

prettyprec :: Int -> Exp -> Doc
prettyprec i (Prim p) = pretty p
prettyprec i (BoolApp Not l) = (pretty Not) <> parens ((pretty.head) l)
prettyprec i (BoolApp b l) = 
  let pb = bopIdx b
      k = prettyOp (pretty b) (map (prettyprec pb) l)
  in if (i < pb || b == AndThen || b == OrElse) then parens k else k 
-- TODO: Parenthesis may be improved. Motivation for the change: casing.adb 
-- from Examiner source code
prettyprec i (ArithApp a l) = 
  let pa = aopIdx a
      k = prettyOp (pretty a) (map (prettyprec pa) l)
  in if i < pa then parens k else k
prettyprec i (RelApp r l) = 
  let pr = ropIdx r
      k = prettyOp (pretty r) (map (prettyprec pr) l)
  in if i < pr then parens k else k
prettyprec i (RangeApp op r e) = 
  prettyprec 1 e <+> pretty op <+> pretty r
prettyprec i (Quantified op exp) =
  pretty op <+> pretty exp

instance Pretty Exp where
  pretty (ArithApp S.Neg l) = t "-" <> parens (pretty l)
  pretty e = prettyprec 1 e

instance Pretty [Exp] where
  pretty = vcat.(map pretty)
  
detlkup :: (Eq a) => [(a, b)] -> a -> b
detlkup l o = (\(Just x) -> x) (lookup o l)
  
instance Pretty BoolOp where
  pretty = detlkup opl
    where
      opl = [(S.Not, t "not"), (S.And, t " and "), (S.Or, t " or "), (S.Xor, t " xor "),
             (S.AndThen, t " and" <+> t "then "), (S.OrElse, t " or" <+> t "else "),
             (S.Implies, t " -> "), (S.Iff, t " <-> ")]
             
instance Pretty ArithOp where
  pretty = detlkup opl
    where
      opl = [(S.Neg, t " - "), (S.Plus, t " + "), (S.Minus, t " - "),
             (S.Mult, t " * "), (S.Div, t " / "), (S.Exp, t " ** "),
             (S.Abs, t " abs "), (S.Cat, t "&"), (S.Mod, t " mod "),
             (S.Rem, t " rem ")]
             
instance Pretty RelOp where
  pretty = detlkup opl
    where
      opl = [(S.Eq, t " = "), (S.Neq, t " /= "), (S.Lt, t " < "), 
             (S.Lte, t " <= "), (S.Gt, t " > "), (S.Gte, t " >= ")]
             
instance Pretty RangeOp where
  pretty IsIn = t "in"
  pretty IsNotIn = t "not" <+> t "in"
            
instance Pretty QualifiedExp where
  pretty (QualExp n e) = pretty n <> t "'" <> pretty e
  pretty (QualAgg n a) = pretty n <> t "'" <> pretty a
  

-----------------------------------------------------------------------------
-- ** Expression Atom

instance Pretty Primary where
  pretty (PrimLit i) = pretty i
  pretty (PrimName n) = pretty n
  pretty (PrimTypeConv ty e) = pretty ty <> parens (pretty e)
  pretty (PrimQualExp q) = pretty q
  -- pretty (PrimExp e) = parens (pretty e)


-----------------------------------------------------------------------------
-- *** Quantifications

instance Pretty QuantOp where
  pretty ForAll = t "for" <+> t "all"
  pretty ForSome = t "for" <+> t "some"
  
instance Pretty QuantExp where
  pretty (QuantExp i n r p)
    = t i <+> t "in" <+> pretty n <+> (prettyPrefix (t "range") r) 
          <+> t "=>" <+> pretty p

-----------------------------------------------------------------------------
-- ** Ranges

instance Pretty Range where
  pretty (RangeStatic e1 e2) = pretty e1 <+> t ".." <+> pretty e2
  pretty (RangeAttrib n  e ) = pretty n <> t "'" <> t "Range" <+> k e
    where
      k Nothing = empty
      k (Just x)= parens (pretty x)

-----------------------------------------------------------------------------
-- ** Literals

instance Pretty Literal where
  pretty (LitInt i)  = pretty i
  pretty (LitReal i) = pretty i
  pretty (LitChar c) = quotes (char c)
  pretty (LitStr s)  = doubleQuotes (t s)
  
instance Pretty (NumLit a) where
  pretty (NumLit _ s) = t s
